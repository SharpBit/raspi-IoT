import dht11
from RPi import GPIO
from flask import Flask, render_template

app = Flask('testapp')


GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)
GPIO.cleanup()


@app.route('/')
def func():
    sensor = dht11.DHT11(pin=14)
    result = sensor.read()
    data = {}

    if result.is_valid():
        data['temp'] = 'Temperature: {} F'.format(result.temperature * 1.8 + 32)
        data['humidity'] = 'Humidity: {}%'.format(result.humidity)

    template_data = {
        'data': data
    }

    return render_template('index.html', **template_data)


if __name__ == '__main__':
    app.run(host='192.168.1.167')
